## Versions

### PPTX

This file was presented on the meetup: [IPv6_home_lab_setup.pptx](IPv6_home_lab_setup.pptx).

### LaTeX version

File [main.tex](main.tex) is pre-release version.
To be updated soon to have the same information as in PPTX version.

How to generate a PDF file from LaTeX:

```sh
sudo apt-get -V --no-install-recommends install texlive-latex-base texlive-latex-extra
pdflatex main.tex
```

Workarounds:
* file `ccicons.sty` is a symlink to `/dev/null` to avoid an error message.
* the pages counter is wrong on the very first PDF build run (because of no cache?).

## Video of the meetup

TBD.

## Additional information

TODO: how to establish simple IPv6 connection locally.

## TODO

+ Add original images.
+ Update the LaTeX version.

## Links

Meetup announcements:
* https://www.meetup.com/dynamic-talks-by-grid-dynamics/events/295821157/
* https://www.eventbrite.co.uk/e/dynamic-talks-meetup-for-devops-tickets-711013790387
* https://dou.ua/calendar/48345/
* https://fb.me/e/3TmzYTXKR

## License

This work is licensed under the Creative Commons Attribution-ShareAlike 4.0 International License. To view a copy of this license, visit http://creativecommons.org/licenses/by-sa/4.0/ or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.
